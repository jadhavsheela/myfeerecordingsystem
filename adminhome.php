<html>
	<head>
	<?php
	session_start();
	if(!isset($_SESSION['admin_username']))
	{
		echo"<script>alert('Login First!');window.location='index.php';</script>";
	}
	?>


	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<style>
        body {
			background-image: url("img/hh.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
	</head>



	<body>
	<?php require "adminmenu.php"; ?>
		<br><br><br><br><br>
		
		<center><h4>Welcome Admin..!! <?PHP echo $_SESSION['admin_username'] ?></h4></center>
		
		<br><br><br><br>
		

	<div class="container">

	<div class="row">
		<div class="col-md-4">
			<div class="card" style="width: 18rem;">
				<img src="img/admin.png" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title">Add Accountant</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="addaccountant.php" class="btn btn-primary">Go</a>
				</div>
			</div>
		</div>


		<div class="col-md-4">
			<div class="card" style="width: 18rem;">
				<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxgafVQ0Qi1P35NmZDzwQxM7WZVRVfB2CCww&usqp=CAU" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title">View Accountant</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="viewaccountants.php" class="btn btn-primary">Go</a>
				</div>
			</div>
		</div>


		<div class="col-md-4">
			<div class="card" style="width: 18rem;">
				<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZhdWkaGO2pGcSa-iNv4DeHmh59mpcA4iA8g&usqp=CAU" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title">Change Password</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="adminchangepassword.php" class="btn btn-primary">Go</a>
				</div>
			</div>
		</div>
	</div>
	</div>
	</body>
</html>