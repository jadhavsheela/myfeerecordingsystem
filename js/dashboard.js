var $toggle = $('#nav-toggle');
var $menu = $('#nav-menu');

$toggle.click(function(e) {
  e.preventDefault();
  $(this).toggleClass('is-active');
  $menu.toggleClass('is-active');
});