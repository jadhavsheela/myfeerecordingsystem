<html>
	<head>
	<?php
	session_start();
	if(!isset($_SESSION['admin_username']))
	{
		echo"<script>alert('Login First!');window.location='index.php';</script>";
	}
	?>


	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script src="js/validation.js"></script>
	<style>
        body {
			background-image: url("img/hh.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
	</head>



	<body>
	<?php require "adminmenu.php"; ?>

	<br><br><br>
	<center><h1> Add Accountant</h1></center>
		<br><br><br><br>
		

	<div class="container">
		<div class="card">
			<div class="card-header">
				Enter Following Data to Add Accountant
			</div>

			<div class="card-body">

			<form action="addaccountantcontroller.php" method="post" onsubmit="return validate();">
				<div class="form-row">
					<div class="form-group col-md-6">
					<label for="inputEmail4">First Name:</label>
					<input type="text" class="form-control" name="fname" id="fname" placeholder="Enter First Name">
					</div>
					<div class="form-group col-md-6">
					<label for="inputPassword4">Last Name:</label>
					<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter Last Name">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputEmail4">Email:</label>
						<input type="email" class="form-control" id="email" name="email"  placeholder="Email">
					</div>
					<div class="form-group col-md-4">
						<label for="inputEmail4">Contact Number:</label>
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
					</div>
					<div class="form-group col-md-2">
						<label for="inputPassword4">Gender:</label>
							<div class="form-check">
								<input class="form-check-input" type="radio" name="gender" value="male" checked>
								<label class="form-check-label" for="gridRadios1">
								Male
								</label>
							</div>
							<div class="form-check">
							<input class="form-check-input" type="radio" name="gender" value="female">
							<label class="form-check-label" for="gridRadios2">
							Female
						</label>
					</div>
				</div>

				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
					<label for="inputEmail4">City:</label>
					<input type="text" class="form-control" id="city" name="city" placeholder="City">
					</div>
					<div class="form-group col-md-6">
					<label for="inputPassword4">Salary:</label>
					<input type="text" class="form-control" id="salary" name="salary" placeholder="Salary">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
					<label for="inputEmail4">Set Password:</label>
					<input type="password" class="form-control" id="pass" name="pass" placeholder="Set Password">
					</div>
					
				</div>

				
			
				
				<div class="form-row">
					<center><button type="submit" class="btn btn-warning">Submit</button></center> <hr>

					<center><button type="reset" class="btn btn-warning">Reset</button></center>

				</div>
				
			</form>

			</div>

		</div>

	</div>
	</body>
</html>