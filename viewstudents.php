<html>
	<head>
	<?php
	session_start();
	if(!isset($_SESSION['acc_email']))
	{
		echo"<script>alert('Login First!');window.location='index.php';</script>";
	}
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script src="js/validation.js"></script>
	<style>
        body {
			background-image: url("img/ss.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
	</head>

	<body>
	<?php require "accountantmenu.php"; ?>

	<br><br><br>
	<center><h1> View Students</h1></center>
		<br><br><br><br>
		

	<div class="container">
		<div class="card">
		<div class="card-header">
				Details of Students can be found here
			</div>

			<div class="card-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">SR.NO</th>
						<th scope="col">Name</th>
						<th scope="col">Email</th>
						<th scope="col">Mobile</th>
						<th scope="col">Address</th>
						<th scope="col">Course</th>
						<th scope="col">Total Fee</th>
						<th scope="col">Due Fee</th>
						<th scope="col">Edit</th>
						<th scope="col">Delete</th>
					</tr>
				</thead>

				<tbody>
				<?php
				require "db.php";
				$obj=new DB();
				$obj->db_connect();
				$records = $obj->viewStudents();
				if($records!=null)
				{
					$i=1;
					while($r = mysqli_fetch_assoc($records)){
				?>
					<tr>
					<form action="viewstudent.php" method="post">
						<td><?php echo $i; ?><input type="hidden" name="id" value="<?php echo $r['student_id']; ?>"></td>
						<td><?php echo $r['student_fname']." ".$r['student_lname'];  ?></td>
						<td><?php echo $r['student_email']; ?></td>
						<td><?php echo $r['student_mobile']; ?></td>
						<td><?php echo $r['student_city']; ?></td>
						<td><?php echo $r['student_course']; ?></td>
						<td><?php echo $r['student_totalfee']; ?></td>
						<td><?php echo $r['student_due']; ?></td>
						<td><button type="submit" value="Edit"  class="btn btn-success">Edit</button></td>
						<td><button type="submit" formaction="deletestudent.php" value="Delete" class="btn btn-danger">Delete</button></td>
						</form>
					</tr>
				<?php
				$i++;
				}
				}
				else
				{
					echo "<script>alert('Data Not Found');window.location='addstudent.php';</script>";
				}
				?>
				<tbody>
				</table>		
		</div>
	</div>
	</body>
</html>