<html>
	<head>
	<?php
	session_start();
	if(!isset($_SESSION['acc_email']))
	{
		echo"<script>alert('Login First!');window.location='index.php';</script>";
	}
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script src="js/validation.js"></script>
	<style>
        body {
			background-image: url("img/ss.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
	</head>

	<body>
	<?php require "accountantmenu.php"; ?>

	<br><br><br>
	<center><h1> Add Student</h1></center>
		<br><br><br><br>
		

	<div class="container">
		<div class="card">
			<div class="card-header">
				Enter Following Data to Add Student 
			</div>

			<div class="card-body">

			<form action="addstudentcontroller.php" method="post" onsubmit="return validate();">
				<div class="form-row">
					<div class="form-group col-md-6">
					<label for="inputEmail4">First Name:</label>
					<input type="text" class="form-control" name="fname" id="fname" placeholder="Enter First Name">
					</div>
					<div class="form-group col-md-6">
					<label for="inputPassword4">Last Name:</label>
					<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter Last Name">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputEmail4">Email:</label>
						<input type="email" class="form-control" id="email" name="email"  placeholder="Email">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4">Contact Number:</label>
						<input type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputEmail4">Course:</label>
						<input type="text" class="form-control" id="course" name="course"  placeholder="SSC/HSC/B.Tech">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4">Course Total Fee:</label>
						<input type="number" class="form-control" id="total" name="total" placeholder="Enter Ammount in INR">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputEmail4">Paid:</label>
						<input type="number" class="form-control" id="paid" name="paid" placeholder="Enter Ammount in INR">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4">Due Ammount:</label>
						<input type="number" class="form-control" id="due" name="due" placeholder="Enter Ammount in INR">
					</div>
				</div>


				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="inputEmail4">Address</label>:</label>
						<input type="text" class="form-control" id="city" name="city">
					</div>
				</div>

				
				<div class="form-row">
					<center><button type="submit" class="btn btn-success">Submit</button></center> <hr>

					<center><button type="reset" class="btn btn-primary">Reset</button></center>

				</div>
				
			</form>

			</div>

		</div>

	</div>
	</body>
</html>