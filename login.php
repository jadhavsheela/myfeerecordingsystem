<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <link rel="stylesheet" href="css/login.css">
    <style>
        body {
            background-image: url("img/pic2.jpg");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
</head>
<body>

<div class="container">
    <br><br><br>
    <div class="row">
        <div class="col-6 login-wrap">
        <h2>Admin Login</h2>
        
        <form action="adminlogin.php" method="post">
        <div class="form">
            <input type="text" placeholder="Username" name="username" />
            <input type="password" placeholder="Password" name="pass" />
            <button type="submit"> Log in </button>
        </div>
        </form>
        </div>

        <br><br><br>




        <div class="col-6 login-wrap">
        <h2>Accountant Login</h2>
        
        <form action="accountantlogin.php" method="post">
        <div class="form">
            <input type="text" placeholder="Username" name="username" />
            <input type="password" placeholder="Password" name="pass" />
            <button type="submit"> Log in </button>
        </div>
        </form>
        </div>

    </div>
</div>
</body>
</html>