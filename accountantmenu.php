<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <a class="navbar-brand" href="accountanthome.php">Fee Recording System | Accountant Login</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
	  	<a class="nav-link" href="addstudent.php">Add Student</a>
      </li>
      <li class="nav-item active">
	  <a class="nav-link" href="viewstudents.php">View Students</a>
      </li>
      <li class="nav-item active">
	  <a class="nav-link" href="accountantchangepassword.php">Change Password</a>
      </li>
    </ul>
    <span class="navbar-text active">
	 <a class="" href="accountantlogout.php">Logout</a>
    </span>
  </div>
</nav>

