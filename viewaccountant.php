<html>
	<head>
	<?php
	session_start();
	if(!isset($_SESSION['admin_username']))
	{
		echo"<script>alert('Login First!');window.location='index.php';</script>";
	}
	?>


	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script src="js/validation.js"></script>
	<style>
        body {
			background-image: url("img/hh.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
	</head>



	<body>
	<?php require "adminmenu.php"; ?>

	<?php
	require "db.php";
	$id=$_POST['id'];
	$obj=new DB();
	$obj->db_connect();
	$data = $obj->viewAccountant($id);
	if($data!=null)
	{
		$r=mysqli_fetch_assoc($data);
	?>

	<br><br><br>
	<center><h5> Editing <?php echo $r['acc_fname']; ?> Accountant Info</h5></center>
		<br><br><br><br>
		

	<div class="container">
		<div class="card">
			<div class="card-header">
				Following Details 
			</div>

			<div class="card-body">

			<form action="editaccountant.php" method="post">
				<div class="form-row">
					<div class="form-group col-md-6">
					<label for="inputEmail4">First Name:</label>
					<input type="text" class="form-control" name="fname" id="fname" value="<?php echo $r['acc_fname']; ?>">
					</div>
					<div class="form-group col-md-6">
					<label for="inputPassword4">Last Name:</label>
					<input type="text" class="form-control" name="lname" id="lname" value="<?php echo $r['acc_lname']; ?>">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputEmail4">Email:</label>
						<input type="email" class="form-control" id="email" name="email"  value="<?php echo $r['acc_email']; ?>">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4">Contact Number:</label>
						<input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $r['acc_mobile']; ?>">
					</div>
					
				</div>

				

				<div class="form-row">
					<div class="form-group col-md-6">
					<label for="inputEmail4">City:</label>
					<input type="text" class="form-control" id="city" name="city" placeholder="City">
					</div>
					<div class="form-group col-md-6">
					<label for="inputPassword4">Salary:</label>
					<input type="text" class="form-control" id="salary" name="salary" placeholder="Salary">
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
					<label for="inputEmail4">Set Password:</label>
					<input type="password" class="form-control" id="pass" name="pass" value="<?php echo $r['acc_pass']; ?>" disabled>
					</div>
					
				</div>

				
			
				
				<div class="form-row">
					<center><button type="submit" class="btn btn-warning">Edit</button></center> <hr>

					<center><button type="reset" class="btn btn-warning">Reset</button></center>

				</div>
				</div>
			</form>

			</div>

		</div>

	</div>

	<?php
}
		?>
	</body>
</html>