<html>
	<head>
	<?php
	session_start();
	if(!isset($_SESSION['admin_username']))
	{
		echo"<script>alert('Login First!');window.location='index.php';</script>";
	}
	?>


	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script src="js/validation.js"></script>
	<style>
        body {
			background-image: url("img/hh.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
	</head>



	<body>
	<?php require "adminmenu.php"; ?>

	<br><br><br>
	<center><h1> View Accountants</h1></center>
		<br><br><br><br>
		

	<div class="container">
		<div class="card">
			<div class="card-header">
				Details of Accountants can be found here
			</div>

			<div class="card-body">

			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">Sr.No.</th>
						<th scope="col">Name</th>
						<th scope="col">Email</th>
						<th scope="col">Mobile</th>
						<th scope="col">City</th>
						<th scope="col">Edit</th>
						<th scope="col">Delete</th>
					</tr>
				</thead>
				<tbody>

				<?php
				require "db.php";
				$obj=new DB();
				$obj->db_connect();
				$records = $obj->viewAccountants();
				if($records!=null)
				{
					$i=1;
					while($r = mysqli_fetch_assoc($records)){
				?>	
					<tr>
					<form action="viewaccountant.php" method="post">
						<td scope="row"><?php echo $i; ?><input type="hidden" name="id" value="<?php echo $r['acc_id']; ?>"></td>
						<td><?php echo $r['acc_fname']." ".$r['acc_lname'];  ?></td>
						<td><?php echo $r['acc_email']; ?></td>
						<td><?php echo $r['acc_mobile']; ?></td>
						<td><?php echo $r['acc_city']; ?></td>
						<td><button type="submit"  value="Edit" class="btn btn-warning">Edit</button></td>
						<td><button type="submit" class="btn btn-danger" formaction="deleteaccountant.php">Delete</button></td>
						</form>
					</tr>
				<?php
				$i++;
				}
				}
				else
				{
					echo "<script>alert('Data Not Found');window.location='addaccountant.php';</script>";
				}
				?>	
					
				</tbody>
			</table>


			
			</div>

		</div>
	</div>
	</body>
</html>