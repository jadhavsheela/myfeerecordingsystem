<nav class="navbar navbar-expand-lg navbar-light bg-warning">
  <a class="navbar-brand" href="adminhome.php">Fee Recording System | Admin Login</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
	  	<a class="nav-link" href="addaccountant.php">Add Accountant</a>
      </li>
      <li class="nav-item active">
	  <a class="nav-link" href="viewaccountants.php">View Accountants</a>
      </li>
      <li class="nav-item active">
	  <a class="nav-link" href="adminchangepassword.php">Change Password</a>
      </li>
    </ul>
    <span class="navbar-text ">
	 <a class="" href="adminlogout.php">Logout</a>
    </span>
  </div>
</nav>
