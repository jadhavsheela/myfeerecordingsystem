<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <link rel="stylesheet" href="css/style.css">

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
    <script  src="js/script.js"></script>

    <title>Fee Recording System</title>
</head>
<body>
<div class="container-fluid main">

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.php">FRS..!!</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#about">About</a></li>
        <li><a href="#">Contact Us</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="myCarousel" class="carousel carousel-fade slide" data-ride="carousel" data-interval="3000">
  <div class="carousel-inner" role="listbox">
    <div class="item active background a"></div>
    <div class="item background b"></div>
    <div class="item background c"></div>
  </div>
</div>

<div class="covertext">
  <div class="col-lg-10" style="float:none; margin:0 auto;">
    <h1 class="title">FEE RECORDING SYSTEM</h1>
    <h3 class="subtitle">A Easy Way to Manage Transactions</h3>
  </div>
  <div class="col-xs-12 explore">
    <a href="#"><a href="login.php"><button type="button"  class="btn btn-lg explorebtn">LOGIN</button></a>
  </div>




</div>
    
<div id="about" class="container page-header">
    <center>  <h1>About</h1></center>
    <br><br>
    <div class="panel panel-success">
    <!-- Default panel contents -->
    <div class="panel-heading">Panel heading</div>
    <div class="panel-body">
        <p>...</p>
    </div>

    <!-- Table -->
    <table class="table">
        ...
    </table>
    </div>


    <div class="panel panel-warning">
    <!-- Default panel contents -->
    <div class="panel-heading">Panel heading</div>
    <div class="panel-body">
        <p>...</p>
    </div>

    <!-- Table -->
    <table class="table">
        ...
    </table>
    </div>

    <div class="panel panel-danger">
    <!-- Default panel contents -->
    <div class="panel-heading">Panel heading</div>
    <div class="panel-body">
        <p>...</p>
    </div>

    <!-- Table -->
    <table class="table">
        ...
    </table>
    </div>
</div>




</body>
</html>